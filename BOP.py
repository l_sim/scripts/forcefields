#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 15:44:09 2021

@author: juliaparedes
"""

from math import *
import numpy as np
import matplotlib.pyplot as plt

class BOP:
    def __init__(self):
        self.__param2 = np.zeros((2,2,2,3))
        self.__param2[0][0][0][0] = 0.7202
        self.__param2[1][1][1][0] = 0.4271

        self.__param2[0][0][0][1] = 0.7947
        self.__param2[1][1][1][1] = 0.8785
        self.__param2[1][0][1][1] = 0.6947
        self.__param2[0][1][0][1] = 0.7658
        self.__param2[0][0][1][1] = 0.8120
        self.__param2[1][0][0][1] = 0.8120
        self.__param2[1][1][0][1] = 0.8729
        self.__param2[0][1][1][1] = 0.8729

        self.__param2[0][0][0][2] = 0.1036
        self.__param2[1][1][1][2] = 0.1458
        self.__param2[1][0][1][2] = 0.0391
        self.__param2[0][1][0][2] = 0.
        self.__param2[0][0][1][2] = 0.1275
        self.__param2[1][0][0][2] = 0.1275
        self.__param2[1][1][0][2] = 0.1642
        self.__param2[0][1][1][2] = 0.1642

        self.__param = np.zeros((2,2,14))
        self.__param[0][0][0] = 2.4235
        self.__param[1][1][0] = 2.1200
        self.__param[1][0][0] = 2.3800
        self.__param[0][1][0] = 2.3800

        self.__param[0][0][1] = 2.4235
        self.__param[1][1][1] = 2.1200
        self.__param[1][0][1] = 2.3800
        self.__param[0][1][1] = 2.3800

        self.__param[0][0][2] = 1.4509
        self.__param[1][1][2] = 1.3059
        self.__param[1][0][2] = 1.9652
        self.__param[0][1][2] = 1.9652

        self.__param[0][0][3] = 0.7255
        self.__param[1][1][3] = 0.6529
        self.__param[1][0][3] = 0.9826
        self.__param[0][1][3] = 0.9826

        self.__param[0][0][4] = 2.6234
        self.__param[1][1][4] = 2.6304
        self.__param[1][0][4] = 2.6234
        self.__param[0][1][4] = 2.6234

        self.__param[0][0][5] = 3.
        self.__param[1][1][5] = 3.
        self.__param[1][0][5] = 3.
        self.__param[0][1][5] = 3.

        self.__param[0][0][6] = 3.7
        self.__param[1][1][6] = 3.65
        self.__param[1][0][6] = 3.7
        self.__param[0][1][6] = 3.7

        self.__param[0][0][7] = 1.5520
        self.__param[1][1][7] = 3.98
        self.__param[1][0][7] = 2.1
        self.__param[0][1][7] = 2.1

        self.__param[0][0][8] = 1.5233
        self.__param[1][1][8] = 3.0877
        self.__param[1][0][8] = 1.7959
        self.__param[0][1][8] = 1.7959

        self.__param[0][0][9] = 0.0975
        self.__param[1][1][9] = 0.9263
        self.__param[1][0][9] = 0.3233
        self.__param[0][1][9] = 0.3233

        self.__param[0][0][10] = 1.5193
        self.__param[1][1][10] = 3.6016
        self.__param[1][0][10] = 0.8534
        self.__param[0][1][10] = 0.8534

        self.__param[0][0][11] = 1.
        self.__param[1][1][11] = 1.
        self.__param[1][0][11] = 1.
        self.__param[0][1][11] = 1.

        self.__param[0][0][12] = 0.4456
        self.__param[1][1][12] = 0.6558
        self.__param[1][0][12] = 0.5
        self.__param[0][1][12] = 0.5

        self.__param[0][0][13] = -25.6485
        self.__param[1][1][13] = 0.76
        self.__param[1][0][13] = 0.
        self.__param[0][1][13] = 0.

    def __f(self, r, i, j, npow = 1.):
        r0 = self.__param[i][j][0]
        rc = self.__param[i][j][1]
        nc = self.__param[i][j][4]
        return pow(r0 * exp(pow((r0/rc), nc) - pow(r/rc, nc))/r, npow)

    def __dfdr(self, r, i, j, npow = 1.):
        r0 = self.__param[i][j][0]
        rc = self.__param[i][j][1]
        nc = self.__param[i][j][4]
        return - npow * self.__f(r, i, j, npow) * (1 / r + nc / rc * pow(r/rc, nc - 1))

    def __spline_f(self, r, i, j, npow):
        r1 = self.__param[i][j][5]
        r_cut = self.__param[i][j][6]
        if r <= r1:
            return self.__f(r, i, j, npow)
        elif r > r_cut:
            return 0.
        else:
            X = self.__f(r1, i, j, npow)
            Y = self.__dfdr(r1, i, j, npow)

            M1=np.array([[1.,r_cut,pow(r_cut,2),pow(r_cut,3)],
                         [0.,1.,2*r_cut, 3*pow(r_cut,2)],
                         [1.,r1,pow(r1,2),pow(r1,3)],
                         [0.,1.,2*r1,3*pow(r1,2)]])
            M2=np.array([[0.],[0.],[X],[Y]])
            sol=np.linalg.solve(M1,M2)
            a=sol[0][0]
            b=sol[1][0]
            c=sol[2][0]
            d=sol[3][0]

            return a+b*r+c*pow(r,2)+d*pow(r,3)
        
    def __beta_sigma(self, r, i, j):
        return self.__param[i][j][8] * self.__spline_f(r, i, j, self.__param[i][j][3])

    def __beta_pi(self, r, i, j):
        return self.__param[i][j][9] * self.__spline_f(r, i, j, self.__param[i][j][3])

    def __g_sigma_jik(self, i, j, k, cos_jik):
        return (1. + self.__param2[j][i][k][1] * (cos_jik - 1.) + self.__param2[j][i][k][2] * (2. * cos_jik * cos_jik - 1)) / (1 + self.__param2[j][i][k][2])

    def __phi_sigma_ij_k(self, i, j, k, r_ij, r_ik, cos_jik):
        return pow(self.__g_sigma_jik(i, j, k, cos_jik)
                   * self.__beta_sigma(r_ik, i, k)
                   / self.__beta_sigma(r_ij, i, j), 2)

    def __ring_sigma_ij_k(self, i, j, k, r_ij, r_ik, r_jk, cos_jik, cos_kji, cos_ikj):
        return (self.__g_sigma_jik(i, j, k, cos_jik)
                * self.__g_sigma_jik(j, k, i, cos_kji)
                * self.__g_sigma_jik(k, i, j, cos_ikj)
                * self.__beta_sigma(r_ik, i, k)
                * self.__beta_sigma(r_jk, j, k)
                / self.__beta_sigma(r_ij, i, j)
                / self.__beta_sigma(r_ij, i, j))

    def __beta_hat_ij_k(self, i, j, k, r_ij, r_ik):
        return (self.__param2[i][i][i][0]
                * pow(self.__beta_sigma(r_ik, i, k)
                      / self.__beta_pi(r_ij, i, j), 2)
                - pow(self.__beta_pi(r_ik, i, k)
                      / self.__beta_pi(r_ij, i, j), 2))

    def __phi_2pi_ij_k(self, i, j, k, r_ij, r_ik, cos_jik):
        return (self.__beta_hat_ij_k(i, j, k, r_ij, r_ik) * (1. - pow(cos_jik, 2))
                + 2 * pow(self.__beta_pi(r_ik, i, k) / self.__beta_pi(r_ij, i, j), 2))

    def __phi_4pi_ij_kq(self, i, j, k, q, r_ij, r_ik, r_jk, r_iq, r_jq,
                        cos_jik, cos_ijk, cos_jiq, cos_ijq, cos_kiq):
        return 0.25 * (
            + pow(self.__beta_hat_ij_k(i, j, k, r_ij, r_ik), 2)
            * pow(self.__beta_hat_ij_k(i, j, q, r_ij, r_iq), 2)
            * (1. - cos_jik * cos_jik)
            * (1. - cos_jiq * cos_jiq)
            + pow(self.__beta_hat_ij_k(i, j, k, r_ij, r_ik), 2)
            * pow(self.__beta_hat_ij_k(j, i, q, r_ij, r_jq), 2)
            * (1. - cos_jik * cos_jik)
            * (1. - cos_ijq * cos_ijq)
            + pow(self.__beta_hat_ij_k(i, j, q, r_ij, r_iq), 2)
            * pow(self.__beta_hat_ij_k(j, i, k, r_ij, r_jk), 2)
            * (1. - cos_ijk * cos_ijk)
            * (1. - cos_jiq * cos_jiq)
            + pow(self.__beta_hat_ij_k(j, i, k, r_ij, r_jk), 2)
            * pow(self.__beta_hat_ij_k(j, i, q, r_ij, r_jq), 2)
            * (1. - cos_ijk * cos_ijk)
            * (1. - cos_ijq * cos_ijq)) * (
                2. * pow(cos_kiq - cos_jiq * cos_jik, 2)
                / (1. - cos_jik * cos_jik) / (1. - cos_jiq * cos_jiq) - 1.
            )

    def setIJ(self, i, j, Vij):
        self.__i = i
        self.__j = j
        self.__Vij = Vij
        self.__Phi  = 0.
        self.__R3   = 0.
        self.__Phi2 = 0.
        self.__Phi4 = 0.
        self.__r_ij = np.linalg.norm(self.__Vij)

        return self.__r_ij <= self.__param[i][j][6]

    def setK(self, k, Vik, Vjk):
        self.__k = k
        self.__Vik = Vik
        self.__Vjk = Vjk

        self.__r_ik = np.linalg.norm(self.__Vik)
        self.__r_jk = np.linalg.norm(self.__Vjk)

        return self.__r_ik <= self.__param[self.__i][k][6] or self.__r_jk <= self.__param[self.__j][k][6]

    def accumulateK(self):
        self.__cos_jik = np.vdot(self.__Vij, self.__Vik) / (self.__r_ik * self.__r_ij)
        self.__cos_ijk = np.vdot(-self.__Vij, self.__Vjk) / (self.__r_jk * self.__r_ij)
        cos_kji = self.__cos_ijk
        cos_ikj = np.vdot(self.__Vik, self.__Vjk) / (self.__r_ik * self.__r_jk)

        self.__Phi += (self.__phi_sigma_ij_k(self.__i, self.__j, self.__k,
                                             self.__r_ij, self.__r_ik, self.__cos_jik)
                       + self.__phi_sigma_ij_k(self.__j, self.__i, self.__k,
                                               self.__r_ij, self.__r_jk, self.__cos_ijk))
        self.__R3  += self.__ring_sigma_ij_k(self.__i, self.__j, self.__k,
                                             self.__r_ij, self.__r_ik, self.__r_jk,
                                             self.__cos_jik, cos_kji, cos_ikj)
        self.__Phi2 += (self.__phi_2pi_ij_k(self.__i, self.__j, self.__k,
                                            self.__r_ij, self.__r_ik, self.__cos_jik)
                        + self.__phi_2pi_ij_k(self.__j, self.__i, self.__k,
                                              self.__r_ij, self.__r_jk, self.__cos_ijk))

    def accumulateKprime(self, q, Viq, Vjq):
        r_iq = np.linalg.norm(Viq)
        r_jq = np.linalg.norm(Vjq)
        if r_iq > self.__param[self.__i][q][6] or r_jq > self.__param[self.__j][q][6]:
            return
        
        cos_jiq = np.vdot(self.__Vij, Viq) / (self.__r_ij * r_iq)
        cos_ijq = np.vdot(-self.__Vij, Vjq) / (self.__r_ij * r_jq)
        cos_kiq = np.vdot(-self.__Vik, Viq) / (self.__r_ik * r_iq)
        self.__Phi4 += self.__phi_4pi_ij_kq(self.__i, self.__j, self.__k, q,
                                            self.__r_ij, self.__r_ik, self.__r_jk, r_iq, r_jq,
                                            self.__cos_jik, self.__cos_ijk,
                                            cos_jiq, cos_ijq, cos_kiq)

    def __Theta_s(self, Theta0, f):
        f0=4.0*(Theta0-5.0/8.0)/3.0

        if Theta0>5.0/8.0 : 
            cs=0.0
        else:
            cs=32.0*(5.0/8.0-Theta0)

        if f>=0 and f<f0:
            Theta_s=2*f
        elif f>=f0 and f<(1-f0):
            F=(f*(1-f)-f0*(1-f0))/pow((1-2*f0),2)
            Theta_s=2*f0+2*F*(1-2*f0)*(1+F*(1-cs*F))
        elif (1-f0)<=f and f<=1 :
            Theta_s=2*(1-f)

        return Theta_s

    def bond_ij(self):
        c_sigma=self.__param[self.__i][self.__j][10]
        f_sigma=self.__param[self.__i][self.__j][12]
        k_sigma=self.__param[self.__i][self.__j][13]
        c_pi=self.__param[self.__i][self.__j][11]

        B_sig_ij = self.__beta_sigma(self.__r_ij, self.__i, self.__j)
        B_pi_ij = self.__beta_pi(self.__r_ij, self.__i, self.__j)

        Theta0 = 1. / sqrt(1 + c_sigma * self.__Phi)
        Theta_sigma = self.__Theta_s(Theta0, f_sigma) * (1. - (f_sigma - 0.5) * k_sigma * self.__R3 / (1. + self.__Phi / 2.))
        Theta_pi= 1. / sqrt(1. + c_pi * (self.__Phi2 / 2. - sqrt(self.__Phi4))) + 1. / sqrt(1. + c_pi * (self.__Phi2 / 2. + sqrt(self.__Phi4)))
        return -0.5*(2*B_sig_ij*Theta_sigma+2*B_pi_ij*Theta_pi)

    def rep_ij(self):
        return 0.5 * self.__param[self.__i][self.__j][7] * self.__spline_f(self.__r_ij, self.__i, self.__j, self.__param[self.__i][self.__j][2])


#---------------------------Fonctions-------------------------

#Matrice d'interaction
def Interactions(R,U): 
#R config et U ensemble des directions genetrices
    A=R
    X=np.array([[0.,0.,0.,0.]])
    for u in np.arange(-1,2):
        for v in np.arange(-1,2):
            for w in np.arange(-1,2):
                for i in np.arange(len(R)):
                    X[0][0]=R[i][0]
                    if(u==0 and v==0 and w==0): break 
                    X[0][1:]=R[i][1:]+u*U[0] +v*U[1]+ w*U[2]
                    A=np.append(A,X,axis=0)
    return A



def Energy(R,A):
    U=0

    bop = BOP()

    for i, Ri in enumerate(R):
        for j, Aj in enumerate(A):
            if i == j:
                continue

            if not bop.setIJ(int(Ri[0]), int(Aj[0]), Aj[1:] - Ri[1:]):
                continue
            
            for k, Ak in enumerate(A):
                if k==j or k==i:
                    continue

                if not bop.setK(int(Ak[0]), Ak[1:] - Ri[1:], Ak[1:] - Aj[1:]):
                    continue

                bop.accumulateK()

                for q, Aq in enumerate(A[k + 1:]):
                    if q + k + 1 == j or q + k + 1 == i:
                        continue
                    bop.accumulateKprime(int(Aq[0]), Aq[1:] - Ri[1:], Aq[1:] - Aj[1:])

            U += bop.bond_ij() + bop.rep_ij()

    return U

#------------------------Matrice Rc : maille cubique (atomes poids 1)----------------

Nc=8
ac=5.653
Rc=np.zeros((Nc,4))
Uc=np.array([[ac,0,0], [0,ac,0],[0,0,ac]])
Pc=np.eye(3)
Dimc=np.array([ac,ac,ac])

#Rc[i][0]==1 -> As
#Rc[i][0]==0 -> Ga

Rc[0]=[1,0,0,0]
Rc[1]=[1,0.5*ac,0.5*ac,0]
Rc[2]=[1,0.5*ac,0,0.5*ac]
Rc[3]=[1,0,0.5*ac,0.5*ac]
Rc[4]=[0,ac/4,ac/4,ac/4]
Rc[5]=[0,3*ac/4,3*ac/4,ac/4]
Rc[6]=[0,3*ac/4,ac/4,3*ac/4]
Rc[7]=[0,ac/4,3*ac/4,3*ac/4]

Ac=Interactions(Rc, Uc)    
Ec=Energy(Rc, Ac)
Vc=pow(ac,3)

#------------------Matrice Rr : configuration maille trigonale ------------
#atomes avec poids 1

Nr=2
ar=ac/sqrt(2)
Rr=np.zeros((Nr,4))
Ur=np.array([[ar,0,0],[0,ar,0],[0,0,ar]])
Dimr=np.array([ar,ar,ar])

Rr[0]=[1,0,0,0]
Rr[1]=[0,ar/4,ar/4,ar/4]

#Matrice projection dans la base cartesienne
Pr=np.zeros((3,3))
c=sqrt(2)/2
Pr[0]=[c,c,0]
Pr[1]=[0,c,c]
Pr[2]=[c,0,c]

#projection de la configuration et des vecteurs generateurs dans la base cartesienne
for i in np.arange(Nr):
    Rr[i][1:]=np.dot(Pr,Rr[i][1:])
Ur=np.dot(Pr,Ur)

Ar=Interactions(Rr, Ur)
Er=Energy(Rr,Ar)
Vr=pow(ar,3)/sqrt(2)


#--------------------------Test ----------------------

print(Ec)
print(Er)
print(Ec/Er)


