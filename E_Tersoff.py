#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
from math import *
import numpy as np
import matplotlib.pyplot as plt

#Unite longeurs : Angstrom

#------------Matrice des parametres---------

class Parameters:
    def __init__(self):
        self.__param=np.zeros((2,2,12))
        #self.__param[0][0][i]=Ga-Ga
        #self.__param[1][1][i]=As-As
        #self.__param[1][0][i]=self.__param[0][1][i]=Ga-As

        #delta_ij
        self.__param[0][0][0]=0.007874 
        self.__param[1][1][0]=0.455
        self.__param[1][0][0]=self.__param[0][1][0]=0.0166

        #S_ij
        self.__param[0][0][1]=1.11 
        self.__param[1][1][1]=1.86
        self.__param[1][0][1]=self.__param[0][1][1]=1.1417

        #beta_ij
        self.__param[0][0][2]=1.08
        self.__param[1][1][2]=1.435
        self.__param[1][0][2]=self.__param[0][1][2]=1.5228

        #Dij
        self.__param[0][0][3]=1.40
        self.__param[1][1][3]=3.96
        self.__param[1][0][3]=self.__param[0][1][3]=2.10

        #R_ij 0
        self.__param[0][0][4]=2.3235 
        self.__param[1][1][4]=2.10
        self.__param[1][0][4]=self.__param[0][1][4]=2.35

        #c_ij
        self.__param[0][0][5]=1.918
        self.__param[1][1][5]=0.1186
        self.__param[1][0][5]=self.__param[0][1][5]=1.29

        #dij
        self.__param[0][0][6]=0.75
        self.__param[1][1][6]=0.1612
        self.__param[1][0][6]=self.__param[0][1][6]=0.56

        #hij=cos(theta_ijk)
        self.__param[0][0][7]= 0.3013
        self.__param[1][1][7]= 0.07748
        self.__param[1][0][7]=self.__param[0][1][7]= 0.237

        #alpha_ij
        self.__param[0][0][8]=1.846
        self.__param[1][1][8]=3.161
        self.__param[1][0][8]=self.__param[0][1][8]=0.0

        #R_ij c
        self.__param[0][0][9]=2.95
        self.__param[1][1][9]=3.1
        self.__param[1][0][9]=self.__param[0][1][9]=3.1

        #D_ij c
        self.__param[0][0][10]=0.15
        self.__param[1][1][10]=0.1
        self.__param[1][0][10]=self.__param[0][1][10]=0.2

        #Gamma_ij
        self.__param[0][0][11]= 1.0
        self.__param[1][1][11]= 1.0
        self.__param[1][0][11]=self.__param[0][1][11]=1.0

    def setIJ(self, i, j):
        self.__i = i
        self.__j = j

    def fcut(self, r):
        Rc = self.__param[self.__i][self.__j][9]
        Dc = self.__param[self.__i][self.__j][10]
        if (r - Rc) <= (-Dc):
            return 1.
        elif abs(r - Rc) < Dc:
            return 0.5*(1-sin(pi*(r-Rc)/(2*Dc)))
        else : 
            return 0.

    def VrVa(self, r):
        D    = self.__param[self.__i][self.__j][3]
        Beta = self.__param[self.__i][self.__j][2]
        S    = self.__param[self.__i][self.__j][1]
        R0   = self.__param[self.__i][self.__j][4]
        return D*exp(-Beta*sqrt(2*S)*(r-R0))/(S-1), S*D*exp(-Beta*sqrt(2/S)*(r-R0))/(S-1)

    def g_ik(self, cos_jik):
        Delta_ik = self.__param[self.__i][self.__j][0]
        c_ik     = self.__param[self.__i][self.__j][5]
        d_ik     = self.__param[self.__i][self.__j][6]
        h_ik     = self.__param[self.__i][self.__j][7]
        return Delta_ik*(1+ pow(c_ik,2)/pow(d_ik,2) - pow(c_ik,2)/(pow(d_ik,2)+pow((h_ik+cos_jik),2)))

    def Khi(self, r, r_ik, cos_jik):
        alpha_ik = self.__param[self.__i][self.__j][8]
        return self.g_ik(cos_jik) * exp(alpha_ik*(r-r_ik))

    def Gamma(self):
        return self.__param[self.__i][self.__j][11]

#---------------Fonctions----------------------

def Energy(R,U,A):
    """
    R: atomic positions and species, 2D array, [[species, x, y, z], ...]
    U: super-cell definition, 3x3 matrix (not used ?)
    A: atomic positions, 2D array, expanded.
    """
    Energie=0
    Param = Parameters()
    
    for i, Ri in enumerate(R):

        for j, Aj in enumerate(A):
            
            if i==j:
                Energie+=0
            else:
                Param.setIJ(int(Ri[0]), int(Aj[0]))

                Vij = (Ri - Aj)[1:]
                r = np.linalg.norm(Vij)
                f = Param.fcut(r)
                if f == 0.:
                    continue
                
                Vr, Va = Param.VrVa(r)
                Gamma = Param.Gamma()

                Khi=0
                for k, Ak in enumerate(A):
                    
                    if i==k or j==k : Khi+=0
                    
                    else:
                        Param.setIJ(int(Ri[0]), int(Ak[0]))
                        
                        Vik = (Ri - Ak)[1:]
                        r_ik = np.linalg.norm(Vik)

                        f_ik = Param.fcut(r_ik)
                        if f_ik == 0.:
                            continue

                        Khi += f_ik * Param.Khi(r, r_ik, np.vdot(Vij,Vik)/(r*r_ik))

                B=pow((1+Gamma*Khi),-0.5) 
                Energie+=0.5*f*(Vr-B*Va)
        
    return Energie



def test(atoms, E_ref):
    E=E_pot(len(atoms),atoms)
    if abs(E-E_ref)>1e-6:
        raise ValueError("test failed")
    

#Dimensions de la boite 
def Dim_bloc(R):
    dim=np.zeros(3)
    for i in np.arange(len(R)):
        if R[i][2]>dim[0] : dim[0]=R[i][2]  # x_max
        if R[i][3]>dim[1] : dim[1]=R[i][3] # y_max
        if R[i][4]>dim[2] : dim[2]=R[i][4] # z_max
    return dim


#Matrice d'interaction
def Interactions(R,U): 
#R config et U ensemble des directions genetrices
    A=R
    X=np.array([[0.,0.,0.,0.]])
    for u in np.arange(-1,2):
        for v in np.arange(-1,2):
            for w in np.arange(-1,2):
                for i in np.arange(len(R)):
                    X[0][0]=R[i][0]
                    if(u==0 and v==0 and w==0): break 
                    X[0][1:]=R[i][1:]+u*U[0] +v*U[1]+ w*U[2]
                    A=np.append(A,X,axis=0)
    return A



#Rayons de coupure, maille cubique
def Cutoffs(Nc,Rc):
    for i in range(Nc):
        if i!=0:break
        for j in range(Nc):
            if i!=j:
                if Rc[i][0]==0 and Rc[j][0]==0: 
                    u=1
                    print("As-As")
                if Rc[i][0] != Rc[j][0]: 
                    u=2
                    print("As-Ga")
                if Rc[i][0]==1 and Rc[j][0]==1: 
                    u=0
                    print("Ga-Ga")
                X=Rc[i][1]-Rc[j][1]
                Y=Rc[i][2]-Rc[j][2]
                Z=Rc[i][3]-Rc[j][3]
                r=sqrt(X**2 + Y**2 + Z**2)
                R_c=Param[u][9]
                Dc=Param[u][10]
                if r-R_c<= (-Dc): f=1
                if abs(r-R_c)<Dc : f=0.5*(1-sin(pi*(r-R_c)/(2*Dc)))
                if r-R_c>Dc : f=0
                print(f)

#augmentation homothetique du volume
def Augmentation(R,Dim,E, V, a,U,P):
    X=np.array([[0.,0.,0.,0.]])
    D=np.dot(Pr,Dim)
    #Nouvelle configuration
    for i in np.arange(3):
        for j in np.arange(len(R)):
            X[0][0]=R[j][0]
            X[0][1:]=R[j][1:]+D[i]
            R=np.append(R,X,axis=0)
        Dim[i]=2*Dim[i]
    
    #Nouveau volume
    V=8*V
    
    #Nouvelles interactions
    A=Interactions(R, U, Dim, a,P)
    
    #Nouvelle energy
    E=Energy(R, U, A)
    
    return (R, V, E, Dim)

def CalculVmin(Ei,Rc,Uc,Ac,ac):
    eps=0.08*ac
    #on fait varier une premiere fois le param de maille
    a_max=ac
    a=a_max-0.05*a_max
    b=a_max+0.05*a_max
    Ea=Energy2(Rc, Uc, Ac, a)
    Eb=Ei
    Vmin=pow(a_max,3)
    if Ea==Eb:
        Vmin=pow(a,3)
        a_max=a
    while abs(a-b)>eps:
        a=a_max-0.05*a_max
        b=a_max+0.05*a_max
        Ea=Energy2(Rc, Uc, Ac, a)
        Eb=Energy2(Rc, Uc, Ac, b)
        if Ea==Ei:
            if Eb==Ei:
                a_max=min(a,b)
        if Ea>Ei:
            if Eb==Ei :
                a_max=min(b,a_max)
        if Ea==Ei:
            if Eb>Ei : 
                a_max=min(a,a_max)
        if Ea<Ei or Eb<Ei:
            print("oups, Ea<Ei")
        Vmin=pow(a_max,3)
    return Vmin

#------------------------Matrice Rc : maille cubique (atomes poids 1)----------------

Nc=8
ac=5.653
Rc=np.zeros((Nc,4))
Uc=np.array([[ac,0,0], [0,ac,0],[0,0,ac]])
Pc=np.eye(3)
Dimc=np.array([ac,ac,ac])

#Rc[i][0]==0 -> As
#Rc[i][0]==1 -> Ga

Rc[0]=[0,0,0,0]
Rc[1]=[0,0.5*ac,0.5*ac,0]
Rc[2]=[0,0.5*ac,0,0.5*ac]
Rc[3]=[0,0,0.5*ac,0.5*ac]
Rc[4]=[1,ac/4,ac/4,ac/4]
Rc[5]=[1,3*ac/4,3*ac/4,ac/4]
Rc[6]=[1,3*ac/4,ac/4,3*ac/4]
Rc[7]=[1,ac/4,3*ac/4,3*ac/4]

# Ac=Interactions(Rc, Uc)
# Ec=Energy(Rc, Uc, Ac)
# Vc=pow(ac,3)



#------------------Matrice Rr : configuration maille trigonale ------------
#atomes avec poids 1

Nr=2
ar=ac/sqrt(2)
Rr=np.zeros((Nr,4))
Ur=np.array([[ar,0,0],[0,ar,0],[0,0,ar]])
Dimr=np.array([ar,ar,ar])

Rr[0]=[0,0,0,0]
Rr[1]=[1,ar/4,ar/4,ar/4]

#Matrice projection dans la base cartesienne
Pr=np.zeros((3,3))
c=sqrt(2)/2
Pr[0]=[c,c,0]
Pr[1]=[0,c,c]
Pr[2]=[c,0,c]

#projection de la configuration et des vecteurs generateurs dans la base cartesienne
for i in np.arange(Nr):
    Rr[i][1:]=np.dot(Pr,Rr[i][1:])
Ur=np.dot(Pr,Ur)

# Ar=Interactions(Rr, Ur)
# Er=Energy(Rr, Ur, Ar)
# Vr=pow(ar,3)/sqrt(2)

#------------------------E(V)---------------------------

# print(Ec)
# print(Er)

# print(Ec/Er)

Energie=[]
Volume=[]
maille=[]

R=np.zeros((Nc,4))
for a in np.arange(0.95*ac,1.05*ac,0.1):
    U=np.array([[a,0,0], [0,a,0],[0,0,a]])
    R[0]=[0,0,0,0]
    R[1]=[0,0.5*a,0.5*a,0]
    R[2]=[0,0.5*a,0,0.5*a]
    R[3]=[0,0,0.5*a,0.5*a]
    R[4]=[1,a/4,a/4,a/4]
    R[5]=[1,3*a/4,3*a/4,a/4]
    R[6]=[1,3*a/4,a/4,3*a/4]
    R[7]=[1,a/4,3*a/4,3*a/4]
    A=Interactions(R, U)
    E=Energy(R, U, A)
    V=pow(a,3)
    maille.append(a)
    Energie.append(E)
    Volume.append(V)
    print(E)
    njko

# # print(Energie)
# print(maille)
plt.plot(maille,Energie)
plt.title("E(V)")
plt.xlabel("Parametre de maille")
plt.ylabel("Energie (eV)")
#plt.legend()
plt.show()





            
            
            
        

    





